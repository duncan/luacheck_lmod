# luacheck_lmod

This project provides a luacheck basic configuration file to define the functions that Lmod provides so that luacheck will not provide warnings against these.

To use:

    cd <folder with Lmod files>
    curl -o .luacheckrc https://git.fmrib.ox.ac.uk/duncan/luacheck_lmod/-/raw/main/luacheckrc?ref_type=heads&inline=false
